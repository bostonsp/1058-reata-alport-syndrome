﻿/*---------------------------------------------------------------------------------
Project:            ____

Program:            ____ 

Function:           ____

Dependencies:		____

Dependents:			____

Author:             ____

Date Completed:     ____

SAS Version:        ____   

Platform:           ____

Macros Called:      ____ 

Macro Parameters:   ____

Comments:           ____

Example:			____

Revisions:
date	name	what
210702	SH		Initial verson
---------------------------------------------------------------------------------*/

* ==================================  Step 1. ICD-10  ================================== *;
proc import out=code1 datafile="C:\Users\seungyoung\Documents\1058-reata-alport-syndrome\Input\CCW Codes of Interest.xlsx" 
	dbms=xlsx replace;
    getnames=yes;
run;
* R = 2097 *;

data code2;
	set code1;

	format Code2 $8.;
	if substr(Code, 4, 1) = "" then Code2 = Code;
	else if substr(Code, 5, 1) = "" then Code2 = substr(Code, 1, 3) || '.' || substr(Code, 4, 1);
	else if substr(Code, 6, 1) = "" then Code2 = substr(Code, 1, 3) || '.' || substr(Code, 4, 2);
	else if substr(Code, 7, 1) = "" then Code2 = substr(Code, 1, 3) || '.' || substr(Code, 4, 3);
	else if substr(Code, 8, 1) = "" then Code2 = substr(Code, 1, 3) || '.' || substr(Code, 4, 4);
run;
* R = 2097 *;

proc sql;
	select n(distinct Code2) as count
	from code2;
quit;
* R = 2093 => 1 ICD code could be associated with multiple diseases *;

proc freq data=code2;
	table condition / missing norow nocol;
run;
/*
Anemia 91 4.34 91 4.34 
Anxiety Disorders 44 2.10 135 6.44 
Cardiovascular Disease 1074 51.22 1209 57.65 
Depressive Disorders 17 0.81 1226 58.46 
Diabetes 206 9.82 1432 68.29 
Heart Failure 38 1.81 1470 70.10 
Hyperlipidemia 6 0.29 1476 70.39 
Hypertension 20 0.95 1496 71.34 
Obesity 21 1.00 1517 72.34 
RA/OA (Rheumatoid Arthritis/ Osteoarthritis) 559 26.66 2076 99.00 
Sensory – Deafness and Hearing Impairment 21 1.00 2097 100.00 
*/

data code3;
	set code2;
	MC1 = 0;
	MC2 = 0;
	MC3 = 0;
	MC4 = 0;
	MC5 = 0;
	MC6 = 0;
	MC7 = 0;
	MC8 = 0;
	MC9 = 0;
	MC10 = 0;
	MC11 = 0;

	if condition = "Anemia" then MC1 = 1;
	if condition = "Anxiety Disorders" then MC2 = 1;
	if condition = "Cardiovascular Disease" then MC3 = 1;
	if condition = "Depressive Disorders" then MC4 = 1;
	if condition = "Diabetes" then MC5 = 1;
	if condition = "Heart Failure" then MC6 = 1;
	if condition = "Hyperlipidemia" then MC7 = 1;
	if condition = "Hypertension" then MC8 = 1;
	if condition = "Obesity" then MC9 = 1;
	if condition = "RA/OA (Rheumatoid Arthritis/ Osteoarthritis)" then MC10 = 1;
	if condition = "Sensory – Deafness and Hearing Impairment" then MC11 = 1;
run;
* R = 2097 *;

proc sql;
	create table code4 as
	select 	Code2,
			max(MC1) as MC1 format=1.,
			max(MC2) as MC2 format=1.,
			max(MC3) as MC3 format=1.,
			max(MC4) as MC4 format=1.,
			max(MC5) as MC5 format=1.,
			max(MC6) as MC6 format=1.,
			max(MC7) as MC7 format=1.,
			max(MC8) as MC8 format=1.,
			max(MC9) as MC9 format=1.,
			max(MC10) as MC10 format=1.,
			max(MC11) as MC11 format=1.
	from code3
	group by Code2
	order by Code2;
quit;
* R = 2093 *;

proc means data=code4 mean min max;
run;

data check;
	set code4;
	if sum(of MC:) > 1;
run;
* R = 4 *;

proc export data=code4
	dbms=csv
	outfile="C:\Users\&SYSUSERID.\Documents\1058-reata-alport-syndrome\Import\MC_Codes.csv"
	replace;
run;
proc export data=code4
	dbms=xlsx
	outfile="C:\Users\&SYSUSERID.\Documents\1058-reata-alport-syndrome\Import\MC_Codes.xlsx"
	replace;
run;

*********************************************  End  *********************************************;
